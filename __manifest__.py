# -*- coding: utf-8 -*-
{
    'name': 'Partner wa.me',
    'version': '12.0.1.1.0',
    'author': 'HomebrewSoft',
    'website': 'https://homebrewsoft.dev/',
    'license': 'LGPL-3',
    'depends': [
        'sms',
    ],
    'data': [
        # security
        # data
        # reports
        # views
        'views/res_partner.xml',
    ],
    'auto_install': True,
}
